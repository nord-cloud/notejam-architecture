# Overview

Notejam is a web application which allows user to sign up/in/out and create/view/edit/delete notes. Notes canbe grouped in pads.

The purpose of this application is to show how cloud native architecture can be leveraged in order to deliver business value with speed in a cost effective manner.

# Requirments

## Initial proposal
The initial architectural proposal is a server side rendered monolithic application 
![](static/old.PNG)
Although there is nothing wrong with starting a business on monolith to get fast to market, some of the requirements imply that there is a need for a desing that works well at scale.

## Improvement suggestion

### High Level Design
In order to take advantage of managed services, reduce operational costs and improve developer experience, while sticking to 5 pillar best cloud practices defined under [the well architected framework](https://aws.amazon.com/architecture/well-architected/?wa-lens-whitepapers.sort-by=item.additionalFields.sortDate&wa-lens-whitepapers.sort-order=desc), I propose moving to a serverless architecture

![](static/proposed-architecture.PNG)

### Argumentation & Requirements break-down

Based on the business requirements we can see why this approach fits better than the initial one. Let's look at them one by one:
> The Application must serve variable amount of traffic. Most users are active during business hours. During big events and conferences the traffic could be 4 times more than typical.

Because the application is not being used outside of business hours, we could implement a server architecture that shuts down automatically to save costs. This implies more work than necessary since we can use a "pay-as-you-go" model when we build the application using serverless.
* lambda functions will allow us to pay ONLY for what we use (we don't call the backend outside of business hours, we don't pay for compute)
* since we don't need a server to be running for the webpage to be served, when using S3 buckets to store the 

Since it is the first time the customer is using the cloud, we can take advantage of the AWS *free tier*, which makes this architecture very attractive.
After the 12 months of the free tier period, a pricing calculation can be offered (by me ofcourse :) ) to the customer.

Pricing details:
* Cloudfront - https://aws.amazon.com/cloudfront/pricing/
* S3 - https://aws.amazon.com/s3/pricing/
* Lambda - https://aws.amazon.com/lambda/pricing/
* dynamoDB - https://aws.amazon.com/dynamodb/pricing/

**NOTE** : It is highly likely that this architecture can be cheaper even after the free tier period since it requires very little operational effort, relying on AWS to handle quite a bit of the heavy lifting for us. Under these conditions the customer can spend effort delivering business value, instead of maintaining tooling.


> The Customer takes guarantee to preserve your notes up to 3 years and recover it if needed.

The chosen database can be used for this. 
A clean-up policy can be added to automatically delete old items. More details on how this can be achieved using dynamoDB can be [read here](https://aws.amazon.com/about-aws/whats-new/2017/02/amazon-dynamodb-now-supports-automatic-item-expiration-with-time-to-live-ttl/)
A NoSQL document database seems to make more sense for this simple use case, rather than a relational one, since we only store document-like data.
Based on a conversation with the customer to clarify the roadmap and other possible requirements not already presented, we could decide to go for something else.

>The Customer ensures continuity in service in case of datacenter failures.

The design does not depend on a self managed datacenter. If "datacenter" is refering to AWS, then Amazon ensures the service availablity in their [SLA](https://aws.amazon.com/lambda/sla/) and [customer agreement](https://aws.amazon.com/agreement/). If we stick to the well architected framework, we need to ensure that the service is resilient across multiple Availability Zones. The services chosen ([for example AWS Lambda](https://docs.aws.amazon.com/lambda/latest/dg/security-resilience.html)) already ensure this.

>The Service must be capable of being migrated to any regions supported by the cloud provider in case of emergency.

The current design satisfies this requirement. Since everything is represented as code, we can deploy in any of the regions made available by Amazon.

> The Customer is planning to have more than 100 developers to work in this project who want to roll out multiple deployments a day without interruption / downtime. 

>The Customer wants to provision separated environments to support their development process for development, testing, production in the near future.

For this purpose we will define CI/CD pipelines that use a version of git-flow allowing for the following functionality:
* feature branch/bugfix - results in a stand alone deployment of the application (frontend or backend)
* master - the application will be deployed to a `staging` environment where all set of tests (like integration, LnP, end2end) will be executed
* tagging the repository with [semantic versioning](https://semver.org/) will result in a deployment to production

All cloud specific resources will be represented as code (either with cloudformation or CDK)

> The Customer wants to see relevant metrics and logs from the infrastructure for quality assurance and security purposes

* We will enable AWS Aplify Analytics & Cloudwatch custom metrics to have all the data needed to make informed decisions
* AWS X-Ray will be used for tracing capabilities to be able to easily debug issues and pin point the exact reason why they occur
* Access logging will be enabled on the exposure layers
* In order to ensure the uptime of the services at all times, we will use AWS Cloudwatch canary to define probes that will tie in with cloudwatch alarms. Based on this information we can enable automation to act accordingly, or define playbooks/runbooks as needed. 

### Database design
For this application we would like to save costs and keep things simple. For this reason we will go with a composite key database design.

We will use one dynamoDB table where the `HASH` key will be the user id. Since this is a 36 character UUID, we can guarantee even distribution, while still keeping responses quick at scale, due to the fact that one user's data will be kept close to each other.

The `RANGE` key will be componsed like follows:
* For a pad entry: `pad_<pad_id>`
* For a note entry: `note_<pad_id>_<note_id>`

This way we can use a `BEGINS_WITH` statement on the `RANGE` key while using a `query` api call, which is much more efficient than a `scan` call.

This design can scale very well as the sharding is distributed evenly and is recommended by AWS in the [AWS re:Invent 2018: Amazon DynamoDB Deep Dive: Advanced Design Patterns for DynamoDB (DAT401)](https://www.youtube.com/watch?v=HaEPXoXVf2k) video

### Possible features idea
We could introduce an attachments feature. We could create an attachments S3 bucket and an user would have access just to a path in the bucket.

The permissions limiting can be done on the cognito level (the authenticated role) by adding an extra policy like so:

```yaml
          - PolicyName: "AllowUserToReadTheirDocumentsOnly"
            PolicyDocument:
              Version: "2012-10-17"
              Statement:
                - Effect: "Allow"
                  Action:
                    - "s3:GetObject"
                  # NOTE: dollar sign separate character to join because serverless cannot escape `$` signs :sigh:
                  Resource: !Join
                    - ''
                    - - 'arn:aws:s3:::'
                      - <bucket name goes here>
                      - '/$'
                      - '{cognito-identity.amazonaws.com:sub}/*'
                - Effect: "Allow"
                  Action:
                    - "s3:ListBucket"
                  Resource:
                  - !Join
                    - ''
                    - - "arn:aws:s3:::"
                      - <bucket name goes here>
                  Condition:
                    StringLike:
                      # NOTE: using Join here because serverless cannot escape `$` signs :sigh:
                      s3:prefix:
                        - !Join
                          - ''
                          - - '$'
                            - '{cognito-identity.amazonaws.com:sub}/'
                        - !Join
                          - ''
                          - - '$'
                            - '{cognito-identity.amazonaws.com:sub}/*'
```

The attachments can be fetched on the frontend using AWS Amplify, specifically the `storage` extention. For more details please head [here](https://docs.amplify.aws/lib/storage/getting-started/q/platform/js)